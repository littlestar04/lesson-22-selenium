package demo;

import org.assertj.core.api.SoftAssertions;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class TestA {
    private WebDriver driver;

    @Before
    public void setup(){
        ChromeOptions options = new ChromeOptions();
        driver = new ChromeDriver();
    }
    @After
    public void tearDown() {
        driver.quit();
    }
    @Test
    public void testFirstScenario(){
        SoftAssertions softAssertions = new SoftAssertions();
        //перейти на сайт
        driver.get("https://homebrandofficial.ru/wear");
        driver.findElement(By.xpath("//*[@id='rec561378393']/div/div/div[6]")).click();
        driver.findElement(By.xpath("//input[@type='text' and @placeholder='Search']")).sendKeys("Лонгслив в красную полоску");
        //String price = driver.findElement(By.xpath("//div[text()='1 результат по запросу: Лонгслив в красную полоску']")).getText();
        //String priceExpected = "1 результат по запросу: Лонгслив в красную полоску";
        WebElement price = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='t-search-widget__result_product-price t-descr t-descr_xs']")));
        Assert.assertTrue(driver.findElement(By.xpath("//div[@class='t-search-widget__result_product-price t-descr t-descr_xs']")).getText().contains("2800"));
        Assert.assertTrue(driver.findElement(By.xpath("//div[@class='t-search-widget__result_product-title t-name t-name_xs']")).getText().contains("Лонгслив в красную полоску"));
        Assert.assertTrue(driver.findElement(By.xpath("//div[@class='t-container t-search-widget__query-result t-descr t-opacity_50']")).getText().contains("1 результат по запросу:"));

    }
}
