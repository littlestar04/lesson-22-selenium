package demo;

import org.assertj.core.api.SoftAssertions;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class TestB {
    private WebDriver driver;
    @Before
    public void setup(){
        ChromeOptions options = new ChromeOptions();
        driver = new ChromeDriver();
    }
    @After
    public void tearDown() {
        driver.quit();
    }
    @Test
    public void testOrderTshirt(){
        SoftAssertions softAssertions = new SoftAssertions();
        driver.get("https://homebrandofficial.ru/wear");
        driver.findElement(By.xpath("//a[@href='https://homebrandofficial.ru/popular/tproduct/561535806-930803998551-futbolka-oversize']"));
        driver.findElement(By.xpath("//a[@href='#order']")).click();
        driver.findElement(By.xpath("//*[@id='rec561378404']/div/div[1]/div[2]/div[1]")).click();
        driver.findElement(By.xpath("//button[@class='t706__sidebar-continue t-btn']")).click();
        driver.findElement(By.xpath("//input[@id='input_1496239431201']"))
                .sendKeys("Иванов Иван Иванович");
        driver.findElement(By.xpath("//*[@id='form561378404']/div[2]/div[2]/div/div[1]/input[2]"))
                .sendKeys("0000000000)");
        driver.findElement(By.xpath("//input[@id='input_1627385047591']"))
                .sendKeys("Армения");
        driver.findElement(By.xpath("//textarea[@name='Адрес для доставки']")).sendKeys("Москва, улица Пушкина");
        driver.findElement(By.xpath("//input[@name='tildadelivery-city']")).click();
        WebElement firstResult = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@data-full-name='Россия, г Москва']")));
        driver.findElement(By.xpath("//div[@data-full-name='Россия, г Москва']")).click();
        driver.findElement(By.xpath("//*[@id='delivery-services-wrapper']/label[1]/div")).click();
        driver.findElement(By.xpath("//input[@name='tildadelivery-userinitials']")).sendKeys("Иванов Иван Иванович");


        driver.findElement(By.xpath("//input[@name='tildadelivery-street']")).click();
        driver.findElement(By.xpath("//input[@name='tildadelivery-street']")).sendKeys("Пушкина");
        WebElement thirdResult = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='street-searchbox']/div[1]/div[2]/div/div[1]")));
        driver.findElement(By.xpath("//*[@id='street-searchbox']/div[1]/div[2]/div/div[1]")).click();
        driver.findElement(By.xpath("//*[@id='house-searchbox']/div[1]/div[1]/div[2]/input")).sendKeys("10");
        driver.findElement(By.xpath("//input[@name='tildadelivery-aptoffice']")).sendKeys("9");
        driver.findElement(By.xpath("//button[text()='ОФОРМИТЬ ЗАКАЗ' and @type='submit']")).click();

        WebElement secondResult = new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.elementToBeClickable(By
                        .xpath("//p[@class='t-form__errorbox-item js-rule-error js-rule-error-phone' and text()='Укажите, пожалуйста, корректный номер телефона']")));
        Assert.assertTrue(driver.findElement(By.xpath("//div[@id='error_1496239478607']")).getText().contains("Укажите, пожалуйста, корректный номер телефона"));
        Assert.assertTrue(driver.findElement(By
                .xpath("//p[@class='t-form__errorbox-item js-rule-error js-rule-error-phone' and text()='Укажите, пожалуйста, корректный номер телефона']"))
                .getText().contains("Укажите, пожалуйста, корректный номер телефона"));

    }

}
